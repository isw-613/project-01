<?php
    //this keeps the session active
    session_start();

    //This is calling the connection file to get into the database
    require "connection.php";

    //this is getting the connection from the file
    $connection = connection();
    
    //this is verifying is we got the admin
    if($_SESSION["usuario"] != "fmurilloalfaro@gmail.com"){
        header("Location: error.php");
    }
    
    //this is gonna get the id from the category
    $id = $_POST["id"];

    //this is gonna verify is there is a product related to the category
    $sql = "SELECT * FROM productos WHERE id_categoria = '$id';";
    $info2 = $connection->prepare($sql); 
    $info2->execute();
    $info = $info2->fetch();

    //this is verifying if the information was sent
    if($_SERVER["REQUEST_METHOD"] == "POST" && $info === false){

        $connection = connection();
        $sql = "DELETE FROM categorias WHERE id = '$id';";
        $connection->query($sql);
        
        header("Location: categories.php");
    }else{
        echo "<script>
        alert('Esta Categoria contiene Productos!');
        window.location.href='categories.php';
        </script>";
    }

?>