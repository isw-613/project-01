<?php
    //this keeps the session active
    session_start();

    //this is gonna bring the functions nedless
    require "connection.php";

    //this realizing the connection
    $connection = connection();

    //this is gonna get the id's sales
    $idventa = $_GET["idventa"];

    //this is bringing the information from the sales
    $sql = "SELECT * FROM ventas WHERE id = :id;";
    $info2 = $connection->prepare($sql); 
    $info2->execute(array(':id' => $idventa));
    $venta = $info2->fetch();
        
    //this is bringing the information from the product
    $sql = "SELECT * FROM productos WHERE id_producto = :id;";
    $info = $connection->prepare($sql); 
    $info->execute(array(':id' => $venta["id_producto"]));
    $producto = $info->fetch();

    //this is gonna get the description of the product
    $sql = "SELECT descri FROM productos WHERE id_producto = :id;";
    $info = $connection->prepare($sql); 
    $info->execute(array(':id' => $venta["id_producto"]));
    $descri = $info->fetch();

    ////Here is called the view of items
    require "views/items.view.php";
?>