<?php
    //this keeps the session active
    session_start();

    //this is gonna bring the functions nedless
    require "connection.php";
    $errores = "";
    //this is gonna verify the info has been sent
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $email = filter_var($_POST["email"],FILTER_SANITIZE_STRING);
        $contra = $_POST["password"];
        $contra = hash('sha512',$contra);

        //this is bringing the connection from the database
        $connection = connection();

        $sql = "SELECT * FROM usuarios WHERE correo = :email AND contra = :con;";
        $info2 = $connection->prepare($sql); 
        $info2->execute(array(':email' => $email,':con' => $contra));
        $info = $info2->fetch();
        if($info === false){
            $errores .= "<li>El Usuario no existe o la información es Incorrecta!</li>";
        }elseif($email == "fmurilloalfaro@gmail.com" && $contra == "c68c9c8258ea7d85472dd6fd0015f047"){
            $_SESSION["usuario"] = $email;
            header("Location: admin.php");
        }
        else{
            $_SESSION["usuario"] = $email;
            header("Location: client.php");
        }
    }

    //Here is called the view of index
    require "views/index.view.php";

?>