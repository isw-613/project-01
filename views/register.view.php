<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/css/register.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/animate.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <title>Registrarse</title>
</head>
<body>
    <div class="container">
        <div class="info wow bounceIn">
            <!-- this is coming back to the beginning -->
            <a href="index.php" style="border-radius: 25px; width:5%; margin-top:10px;" title="Regresar" class="btn"><i class="material-icons">arrow_back</i></a>

            <h2>Registrarse!</h2>
            <div class="row">
                <!-- this form will call the php file to interact -->
                <form class="col s12" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    <div class="row">
                        <div class="input-field col s4">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" name="nombre" class="validate" required>
                        <label for="icon_prefix">Nombre</label>
                        </div>
                        <div class="input-field col s4">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="lastname" type="text" name="apellido" class="validate" required>
                        <label for="lastname">Apellido</label>
                        </div>
                        <div class="input-field col s4">
                        <i class="material-icons prefix">phone</i>
                        <input id="icon_telephone" type="number" name="telefono" class="validate" required>
                        <label for="icon_telephone">Teléfono</label>
                        </div>
                        <div class="input-field col s6">
                        <i class="material-icons prefix">mail</i>
                        <input id="correo" type="email" name="correo" class="validate" required>
                        <label for="correo">Correo</label>
                        </div>
                        <div class="input-field col s6">
                        <i class="material-icons prefix">location_on</i>
                        <input id="dire" type="text" name="dire" class="validate" required>
                        <label for="dire">Dirección</label>
                        </div>
                        <div class="input-field col s6">
                        <i class="material-icons prefix">lock</i>
                        <input id="pass" type="password" name="contra" class="validate" required>
                        <label for="pass">Contraseña</label>
                        </div>
                        <div class="input-field col s6">
                        <i class="material-icons prefix">lock</i>
                        <input id="cpass" type="password" name="contra2" class="validate" required>
                        <label for="cpass">Confirmar contraseña</label>
                        </div>
                    </div>
                    <div class="error">
                        <ol>
                        <?php if(!empty($errores)):?>
                            <?php echo $errores;?>
                        <?php endif;?>
                        </ol>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Registrar usuario
                        <i class="material-icons right">Enviar</i>
                    </button>
                </form>
            </div>
        </div>

    </div>
    <script src="./assets/js/jquery.js"></script>
    <script src="./assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
</body>
</html>