<?php 
    //this keeps the session active
    session_start();

    //this is gonna verify if there is an active session
    if($_SESSION["usuario"] != "fmurilloalfaro@gmail.com"){
        header("Location: error.php");
    }

    //Here is called the view of admin
    require "views/admin.view.php";
?>
