<?php 
    //this keeps the session active
    session_start();

    //This is calling the connection file to get into the database
    require "connection.php";

    //this is verifying is we got the admin
    if($_SESSION["usuario"] != "fmurilloalfaro@gmail.com"){
        header("Location: error.php");
    }

    //this is verifying if the information was sent
    if($_SERVER["REQUEST_METHOD"] == "POST"){
    
        $id = $_POST["id"];

        $connection = connection();
        $sql = "DELETE FROM productos WHERE id_producto = '$id';";
        $connection->query($sql);
        
        header("Location: products.php");
    }

?>