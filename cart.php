<?php 
    //this keeps the session active
    session_start();

    //this is gonna bring the functions nedless
    require "connection.php";

    //this realizing the connection
    $connection = connection();

    //this is bringing the information from the user
    $sql = "SELECT id FROM usuarios WHERE correo = :correo;";
    $info2 = $connection->prepare($sql); 
    $info2->execute(array(':correo' => $_SESSION["usuario"]));
    $id = $info2->fetch();

    //this is bringing the information from the user
    $sql = "SELECT * FROM ventas WHERE id_cliente = :id ORDER BY id DESC;";
    $info2 = $connection->prepare($sql); 
    $info2->execute(array(':id' => $id["id"]));
    $cliente = $info2->fetchAll();
    
    //Here is called the view of cart
    require "views/cart.view.php";
?>